//
//  LoangeController.swift
//  TimeCaps
//
//  Created by Daniel  Haas on 18.04.17.
//  Copyright © 2017 Daniel Haas. All rights reserved.
//

import UIKit
import CloudKit
class LoangeController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    var startYear: Int = 10000
    
    var heightYearCell : CGFloat = 25
    
    var heightNormalCell : CGFloat = 20
    
    var heightMiddleCell : CGFloat = 35
    
    var timeCapsData : [CKRecord] = []
    
    let initialYear  = 2018
    
    @IBOutlet var TimeCollectionView: UICollectionView!
    
    @IBOutlet var InfoCollectionView: UICollectionView!

    @IBOutlet weak var clctnViewFlowLayout: UICollectionViewFlowLayout!
    
    
    //MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        LayoutSize()
        self.registerNibs()
        
        let adjustedInsets : CGFloat = UIScreen.main.bounds.width / 2 - 25
        self.TimeCollectionView.contentInset = UIEdgeInsetsMake(0, adjustedInsets, 0, adjustedInsets)
        self.fetchDataFromCloudKit(year: initialYear)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let item = self.collectionView(self.TimeCollectionView!, numberOfItemsInSection: 0) - 1
        let lastItem = IndexPath(item: item, section: 0)
        self.TimeCollectionView.scrollToItem(at: lastItem, at: .right, animated: false)
        self.TimeCollectionView.performBatchUpdates({
        }, completion: {
            (_ ) in
            
            self.StartAnimating(lastItem)
        })
    }
    
    
    func registerNibs()
    {
        self.InfoCollectionView.transform = CGAffineTransform(scaleX: 1, y: -1)
         /********** First Info Cell is for Movie Cell ***************/
        self.InfoCollectionView.register(UINib(nibName : "FirstInfoCell", bundle : nil ), forCellWithReuseIdentifier: "FirstInfoCell")
    
        /*********** Second Info Cell is for Info Cell ***************/
        self.InfoCollectionView.register(UINib(nibName : "SecondInfoCell", bundle : nil ), forCellWithReuseIdentifier: "SecondInfoCell")
        
        /*********** Third Info cell is for Music Cell ***************/
        self.InfoCollectionView.register(UINib(nibName : "ThirdInfoCell", bundle : nil ), forCellWithReuseIdentifier: "ThirdInfoCell")
        
        /********** Forth Info Cell is for President Cell ***********/
        self.InfoCollectionView.register(UINib(nibName : "FourthInfoCell", bundle : nil ), forCellWithReuseIdentifier: "FourthInfoCell")
        
        /********* More Info cell is for More Button **************/
        self.InfoCollectionView.register(UINib(nibName : "MoreInfoCell", bundle : nil ), forCellWithReuseIdentifier: "MoreInfoCell")
        
    }
    
    
    //MARK: CollectionView Data Source and Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == TimeCollectionView
        {
            return 12019
        }
        else
        {
            return 6
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == TimeCollectionView
        {
            let cell: TimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath) as! TimeCell
            
            let mainYear = self.startYear - indexPath.row
            
            if indexPath.row % 10 == 0
            {
                cell.yearLbl.font = UIFont.systemFont(ofSize: 14, weight: 0.3)
                cell.heightConstant.constant = heightYearCell
                cell.isYearCell = true
            }
            else
            {
                cell.yearLbl.font = UIFont.systemFont(ofSize: 13)
                cell.heightConstant.constant = heightNormalCell
                cell.isYearCell = false
            }
            cell.querLine.layer.cornerRadius = 1
            if mainYear <= 0 {
                cell.yearLbl.text = "\(abs(mainYear))"
            } else {
                cell.yearLbl.text = "\(mainYear)"
            }
            
            return cell
        }
        else
        {
            if indexPath.item == 0
            {
                let cell = self.InfoCollectionView.dequeueReusableCell(withReuseIdentifier: "FirstInfoCell", for: indexPath) as! FirstInfoCell
                cell.transform = CGAffineTransform(scaleX: 1, y: -1);
                if timeCapsData.count > 0
                {
                    cell.presidentArray = self.timeCapsData[0].value(forKey: "presidentArray") as! [String]
                }
                return cell
            }
            else if indexPath.item == 1
            {
                let cell = self.InfoCollectionView.dequeueReusableCell(withReuseIdentifier: "SecondInfoCell", for: indexPath) as! SecondInfoCell
                cell.transform = CGAffineTransform(scaleX: 1, y: -1)
                return cell
            }
            else if indexPath.row == 2
            {
                let cell = self.InfoCollectionView.dequeueReusableCell(withReuseIdentifier: "ThirdInfoCell", for: indexPath) as! ThirdInfoCell
                cell.transform = CGAffineTransform(scaleX: 1, y: -1);
                cell.title.text = "Music Chart"
                if self.timeCapsData.count > 0
                {
                    cell.musicArray = self.timeCapsData[0].value(forKey: "MusicList") as! [String]
                }
                return cell
            }
            else if indexPath.row == 3
            {
                let cell = self.InfoCollectionView.dequeueReusableCell(withReuseIdentifier: "ThirdInfoCell", for: indexPath) as! ThirdInfoCell
                cell.transform = CGAffineTransform(scaleX: 1, y: -1);
                cell.title.text = "President"
                cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
                cell.contentView.borderWidth = 1.0
                cell.contentView.layer.cornerRadius = 5.0
                return cell
            }
                
            else if indexPath.row == 4
            {
                let cell = self.InfoCollectionView.dequeueReusableCell(withReuseIdentifier: "MoreInfoCell", for: indexPath) as! MoreInfoCell
                cell.transform = CGAffineTransform(scaleX: 1, y: -1);
                return cell
            }
            else
            {
                let cell = self.InfoCollectionView.dequeueReusableCell(withReuseIdentifier: "FourthInfoCell", for: indexPath) as! FourthInfoCell
                cell.transform = CGAffineTransform(scaleX: 1, y: -1);
                cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
                cell.contentView.borderWidth = 1.0
                cell.contentView.layer.cornerRadius = 5.0
                return cell
            }
        }
    }
    
    
    /****************** Size for Every Cell ***********************/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == InfoCollectionView
        {
            if indexPath.item == 0
            {
                return CGSize(width: screenwidth/3 - 25 , height: 335)
            }
            if indexPath.item == 1
            {
                return CGSize(width: screenwidth/3 + 25, height: 335)
            }
            if indexPath.item == 2
            {
                return CGSize(width: screenwidth/3 - 25, height: 335)
            }
            
            if indexPath.item == 3
            {
                return CGSize(width: screenwidth/3 - 25, height: 0)
            }
            if indexPath.item == 4
            {
                return CGSize(width: screenwidth/3 + 25, height: 0)
            }
            if indexPath.item == 5
            {
                return CGSize(width:screenwidth/3 - 25, height: 0)
            }
            
            return CGSize(width: 200, height: 250)
        }
        return CGSize(width: 50  , height: 60)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt indexPath: NSInteger) -> UIEdgeInsets {
        if collectionView == InfoCollectionView
        {
            if indexPath == 4
            {
                return UIEdgeInsets(top: 1000, left: 0, bottom: 100, right: 0)
                
            }
            else
            {
                return UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
                
            }
        }
        return UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.TimeCollectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
    }
    
    
    
    // MARK: Custom Funtions For Animate in CollectionView
    func StartAnimating(_ indexPath : IndexPath)
    {
        for i in TimeCollectionView.indexPathsForVisibleItems
        {
            guard let cell = TimeCollectionView.cellForItem(at: i) as? TimeCell else{ return }
            
            if i == indexPath
            {
                UIView.transition(with: cell, duration: 0.4, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    
                    cell.heightConstant.constant = self.heightMiddleCell
                    
                    cell.yearLbl.font = UIFont.systemFont(ofSize: 15, weight: 0.7)
                    
                    cell.layoutIfNeeded()
                    
                }, completion: nil)
            }
            else if cell.heightConstant.constant == heightMiddleCell
            {
                if cell.isYearCell
                {
                    UIView.transition(with: cell, duration: 0.4, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        
                        cell.heightConstant.constant = self.heightYearCell
                        
                        cell.yearLbl.font = UIFont.systemFont(ofSize: 14, weight: 0.1)
                        
                    }, completion: nil)
                }
                else
                {
                    UIView.transition(with: cell, duration: 0.4, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        
                        cell.heightConstant.constant = self.heightNormalCell
                        cell.yearLbl.font = UIFont.systemFont(ofSize: 13)
                        
                        cell.layoutIfNeeded()
                        
                    }, completion: nil)
                }
            }
        }
    }
    
    
    private var lastIndexPath = IndexPath()
    
    // MARK: ScrollView Delegates
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let center = self.view.convert(self.TimeCollectionView.center, to: self.TimeCollectionView)
        if let indexPath = self.TimeCollectionView.indexPathForItem(at: center)
        {
            self.StartAnimating(indexPath)
            lastIndexPath = indexPath
        }
    }

    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let middleIndexPath = lastIndexPath.row%10000
        if (middleIndexPath) > MINIMUM_YEAR
        {
            self.fetchDataFromCloudKit(year: middleIndexPath)
        }
        self.TimeCollectionView.scrollToItem(at: lastIndexPath, at: UICollectionViewScrollPosition(), animated: false)
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if !decelerate
        {
           self.TimeCollectionView.scrollToItem(at: lastIndexPath, at: UICollectionViewScrollPosition(), animated: false)
        }
    }
    
    //MARK: Layout Sizes For Every iPhone
    func LayoutSize ()
    {
        TimeCollectionView.backgroundColor = oldbrown
        // Iphone 3.5
        if UIScreen.main.bounds.maxY == 480
        {
            
        }
        
        // Iphone 4
        if UIScreen.main.bounds.maxY == 568
        {
            
        }
        
        // Iphone 4.7
        if UIScreen.main.bounds.maxY == 667
        {
            
        }
        
        // Iphone 5.5
        if UIScreen.main.bounds.maxY == 736 {
            
        }
    }

    
    //MARK: Fetch Data from iCloud
    private func fetchDataFromCloudKit(year : Int)
    {
        let query = CKQuery(recordType: TABLE_NAME, predicate: NSPredicate(format: "Year = %d",year))
        CLOUD_KIT_DATABASE.perform(query, inZoneWith: nil) { (records, error) in
            print(error?.localizedDescription ?? "")
            if (error == nil)
            {
                self.timeCapsData.removeAll()
                self.timeCapsData = records!
                DispatchQueue.main.async
                {
                    self.InfoCollectionView.reloadData()
                }
            }
        }
    }
}





