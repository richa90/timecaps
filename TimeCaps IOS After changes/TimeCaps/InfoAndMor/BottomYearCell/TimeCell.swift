//
//  TimeCell.swift
//  TimeCaps
//
//  Created by Daniel  Haas on 19.04.17.
//  Copyright © 2017 Daniel Haas. All rights reserved.
//

import UIKit

class TimeCell: UICollectionViewCell
{
    
    public var isYearCell = false
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet var horizonLine: UIView!
    @IBOutlet var querLine: UIView!
    
    

    @IBOutlet weak var heightMiddleCell: UIView!

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    
}
