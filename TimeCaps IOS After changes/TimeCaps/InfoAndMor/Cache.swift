//
//  Cache.swift
//  TimeCaps
//
//  Created by Daniel  Haas on 18.04.17.
//  Copyright © 2017 Daniel Haas. All rights reserved.
//

import UIKit
import CloudKit

let oldbrown = UIColor(red: 0.92, green: 0.80, blue: 0.56, alpha: 1.00)

let screenwidth = UIScreen.main.bounds.size.width

let CLOUD_KIT_DATABASE = CKContainer.default().publicCloudDatabase

let TABLE_NAME = "TimeCapsData"

let YEAR_KEY = "Year"

let MINIMUM_YEAR = 2000

let MAXIMUM_YEAR = 2018
