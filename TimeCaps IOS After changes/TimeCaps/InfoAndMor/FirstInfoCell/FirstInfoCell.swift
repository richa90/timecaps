//
//  FirstInfoCell.swift
//  TimeCaps
//



import UIKit

class FirstInfoCell: UICollectionViewCell  , UITableViewDataSource , UITableViewDelegate{
    @IBOutlet var appMusicTableView : UITableView!
     @IBOutlet var musicTableView : UITableView!
    
        
    
    var presidentArray : [String] = []
    {
        didSet
        {
            if presidentArray.count > 0
            {
                DispatchQueue.main.async
                    {
                        self.appMusicTableView.reloadData()
                }
            }
        }
    }

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.appMusicTableView.estimatedRowHeight = 40
        self.appMusicTableView.rowHeight = UITableViewAutomaticDimension
        self.appMusicTableView.register(UINib(nibName : "MusicChartCell", bundle : nil ), forCellReuseIdentifier: "MusicChartCell")

           self.musicTableView.register(UINib(nibName : "MovieInfoCell", bundle : nil ), forCellReuseIdentifier: "MovieInfoCell")
        
        // Initialization code
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return presidentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == musicTableView
        {
            let cell = self.musicTableView.dequeueReusableCell(withIdentifier: "MovieInfoCell", for: indexPath) as! MovieInfoCell
            return cell
        }
        else
        {
            let cell = self.appMusicTableView.dequeueReusableCell(withIdentifier: "MusicChartCell", for: indexPath) as! MusicChartCell
            if presidentArray.count > 0
            {
                cell.musicLabel.text = presidentArray[indexPath.row]

            }
                return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == musicTableView
        {
            return 160
        }
        else
        {
           return UITableViewAutomaticDimension
        }
    }
}
