//
//  FourthInfoCell.swift
//  TimeCaps
//

import UIKit

class FourthInfoCell: UICollectionViewCell,   UITableViewDataSource , UITableViewDelegate
{

    @IBOutlet var appTableView : UITableView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
         self.appTableView.register(UINib(nibName : "AppInfoCell", bundle : nil ), forCellReuseIdentifier: "AppInfoCell")
        // Initialization code
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.appTableView.dequeueReusableCell(withIdentifier: "AppInfoCell", for: indexPath) as! AppInfoCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 160
        
    }

}
