//
//  ThirdInfoCell.swift
//  TimeCaps
//

import UIKit

class ThirdInfoCell: UICollectionViewCell ,  UITableViewDataSource , UITableViewDelegate
{
    @IBOutlet var title : UILabel!
    @IBOutlet var appMusicTableView : UITableView!
    
    @IBOutlet var appTableView : UITableView!
    
    var musicArray : [String] = []
    {
        didSet{
            if musicArray.count > 0
            {
                DispatchQueue.main.async {
                    self.appMusicTableView.reloadData()
                }
                
            }
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.appMusicTableView.estimatedRowHeight = 40
        self.appMusicTableView.rowHeight = UITableViewAutomaticDimension
        self.appMusicTableView.register(UINib(nibName : "MusicChartCell", bundle : nil ), forCellReuseIdentifier: "MusicChartCell")
        self.appTableView.register(UINib(nibName : "AppInfoCell", bundle : nil ), forCellReuseIdentifier: "AppInfoCell")
        // Initialization code
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return musicArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == appMusicTableView
        {
            let cell = self.appMusicTableView.dequeueReusableCell(withIdentifier: "MusicChartCell", for: indexPath) as! MusicChartCell
            
            
            cell.musicLabel.text = musicArray[indexPath.row]
            return cell
        }
        else
        {
            let cell = self.appTableView.dequeueReusableCell(withIdentifier: "AppInfoCell", for: indexPath) as! AppInfoCell
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == appTableView
        {
            return 160
        }
        else
        {
            return UITableViewAutomaticDimension
        }
    }
}
